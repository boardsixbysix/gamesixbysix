_log = require('../lib/logger.coffee') 'Level'
Unit = require '../models/Unit'
Item = require '../models/Items/Item'
Environment = require '../entity/Environment'
StoryResurrectionService = require '../basic/StoryResurrectionService'
ActiveCellsControl = require '../basic/ActiveCellsControl'
PlayersController = require '../controllers/PlayersController'
UnitController = require '../controllers/UnitController'
DiceController = require '../controllers/DiceController'
InventoriesController = require '../controllers/InventoriesController'
OneHitCombatRoutine = require '../basic/combat/routine/OneHitCombatRoutine'
ItemsController = require '../controllers/ItemsController'

DiceObserver = require '../views/observers/DiceObserver'
PlayerObserver = require '../views/observers/PlayerObserver'
OneHitTrapObserver = require '../views/observers/OneHitTrapObserver'
OgrCombatRoutine = require '../basic/combat/routine/OgrCombatRoutine'
GameSessionController = require '../controllers/GameSessionController'
SessionObserver = require '../views/observers/SessionObserver'
Inventory = require '../models/Inventory'

FrameView = require '../views/FrameView'
{ random } = require '../lib/util'

WorldMap = require '../pathing/WorldMap'
Cell = require '../models/Cell'

module.exports = class Level extends Phaser.State
	#gameSessionController: null
	
	@environment: null
	isFinish: null

	create: ->
		#Инициализация рамки интерфейса
		frameView = new FrameView()

		@stage.backgroundColor = '#ffffff'
		@game.add.image 0, 0, 'level'
		#@game.add.button 600, 600, 'cube', @actionOnClick, @, 2, 1, 0

		game.worldMap = new WorldMap @game
		
		game.itemsController = new ItemsController()
		
		game.gameSessionController = new GameSessionController
		game.gameSessionController.attach new SessionObserver

		game.diceController = new DiceController
		game.diceController.attach new DiceObserver 678, 460, "cube", "cubeBlock", @actionOnClick
		game.environment = new Environment

		game.inventoriesController = new InventoriesController
		game.inventoriesController.put new Inventory 1
		game.inventoriesController.put new Inventory 2

		game.playersController = new PlayersController()

		jhon = Unit.createStoryPlayer(1,'John', 10,0, 0, 1, 0, 0, 0)
		wick = Unit.createStoryPlayer 2, 'Wick', 10, 0, 0, 1, 0, 0, 0
		
		game.playersController.put jhon
		game.playersController.put wick
		
		game.gameSessionController.addPlayer jhon
		game.gameSessionController.addPlayer wick
		#ogre = new UnitController Unit.createAggressive( 3, 'Ogre', 10, 1, 1, 1 ), 4, 600, 150
		#ogre.attach new UnitObserver().init 600, 150, 'ogrTest1'
		#ogre.setCombatRoutine new OgrCombatRoutine

		#ogre.updateView()

		#trap = new UnitController Unit.createAggressive( 4, 'Ice Clow', 10, 1, 4, 1, 10,10), 4, 200, 10
		#trap.attach new OneHitTrapObserver().init 200, 10, 'TrapIceClow'
		#trap.setCombatRoutine new OneHitCombatRoutine
		#_log trap

		#game.environment.addUnitController ogre
		#game.environment.addTrapController trap

		#trap1.initDraw 400,0, 'TrapIceClow'
		#game.environment.addTrapController trap1
		
		game.gameSessionController.startProcess()

		game.playersController.changeCellEvent null, game.worldMap.mapPath[0]

		game.diceController.switchDiceOn game.gameSessionController.getCurrentPlayer()
		#game.environment.addUnit( Unit.createAggressive('ogrTest1', 'Огр', 200, 140, new CombatRouting ,new ActiveCellsControl(3), 10, 0, 8, 1))
		game.resurrectionService = new StoryResurrectionService

		return

	actionOnClick: ->
			_log 'Нажатие на кнопку'
			game.diceController.throwDice( )
			.then ->
				game.diceController.updateView()
			.then ->
				_log 'бросок'
				game.session.current.executeRoutine(game.diceController.getDiceRoll())
			.then ->
				game.session.current.updateView()
			.then ->
				if game.session.isLastStep()
					_log 'обработка последнего хода юнита'
					game.environment.ExecuteStep()
					.then ->
						_log 'обработка последнего хода для сервиса воскрешения'
						game.resurrectionService.updateForPlayers()
			.then ->
				_log 'переключение игроков'
				#game.session.current.checkLocation()
				game.environment.initTraps()
				_log game.environment
			#Очистка у доп. объектов
			#Инициализация если все выполнено
			.then ->
				game.session.switchPlayer()
			.done()
			return

	update : ->
		#if game.session.isFinishEveryone()
		#	_log 'Все финишировали'
		#	@game.state.start 'Credits', yes, no
		if game.resurrectionService.isResurrectionEveryone()
			@game.state.start 'Credits', yes, no
		return

	render : ->
		_log 'Сработал цикл рендера'
		game.gameSessionController.updateView()
		.then ->
			game.diceController.updateView()
		.then ->
			game.playersController.updateViews()
