module.exports = class Preloader extends Phaser.State

	preload: ->
		#  Set-up our preloader sprite
		@preloadBar = @add.sprite 200, 250, 'preloader'
		@load.setPreloadSprite @preloadBar

		# Load our actual games assets
		@load.image 'level', 'assets/level.png'
		@load.image 'player', 'assets/gaagb.png'
		@load.image 'cube', 'assets/cube.png'
		@load.image 'cubeBlock', 'assets/cubeBlock.png'
		@load.image 'cell', 'assets/cell.png'
		@load.image 'startCell', 'assets/startElement.png'
		@load.image 'stopCell', 'assets/stopcell.png'
		@load.image 'ogrTest1', 'assets/ogr_test_1.png'
		@load.image 'preAggroCell', 'assets/preAggroCell.png'
		@load.image 'finishCell', 'assets/finishElement.png'
		@load.image 'OnePartTestTrap', 'assets/cellOnePartTestTrap.png'
		@load.image 'TwoPartTestTrap', 'assets/cellTwoPartTestTrap.png'
		@load.image 'TrapIceClow', 'assets/TrapIceClow.png'
		@load.image 'FrameCube', 'assets/frameDice.png'
		@load.image 'LineOnDownRight', 'assets/lineOnDownRight.png'
		@load.image 'LineDown', 'assets/lineDown.png'
		@load.image 'LineDownLeft', 'assets/lineDownLeft.png'
		@load.image 'EdgePanel', 'assets/edgePanel.png'
		@load.image 'Movement', 'assets/movement.png'
		@load.image 'Shield', 'assets/shield.png'
		@load.image 'Armor', 'assets/armor.png'
		@load.image 'Food', 'assets/food.png'

		game.load.script('webfont', '//ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js');

		return

	create: ->
		tween = @add.tween this.preloadBar
		.to { alpha: 0 }, 100, Phaser.Easing.Linear.None, yes

		tween.onComplete.add @startMainMenu, @
		return

	startMainMenu: ->
		@game.state.start 'Level', yes, no
		return
