module.exports = class Boot extends Phaser.State

	preload: ->
		@load.image 'preloader', 'assets/loader.png'
		return

	create: ->
		@game.state.start 'Preloader', yes, no
		return

