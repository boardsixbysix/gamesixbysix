_log = require('../lib/logger.coffee') 'UnitView'
Q = require 'q'
module.exports = class UnitView
	constructor: ( x, y, @_imageName)->
			@group = game.add.group()
			@group.position.set x, y
			@_sprite = game.add.sprite 0, 0, @_imageName, null, @group
			style =
				font: 'Merriweather'
				stroke: '#000000'
				fontSize: 20
			@_label = game.add.text 10, -20, " ", style, @group

	_imageName: null
	_sprite: null
	group:null
	_label: null

	getHeight: ( x, y)->
		return @_sprite.height + 20

	update: ( state )->
		_log "[VIEW]Начала обновления юнита со статами #{state.name}, #{state.hp} для юнита "+@_imageName
		deferred = Q.defer()
		_log  state.isDead
		if state.isDead
			_log "[VIEW]Юнит мертв. Уничтожение группы"
			@group.destroy()
			return
		_log "Обновление надписи на #{state.name}, #{state.hp} HP"
		@_label.setText "#{state.name}, #{state.hp} HP"

		if state.destPoints.length is 0
			return Q()
		tweens = []
		_log state.destPoints
		for i in [ 0...state.destPoints.length ]
			if i is 0
				tweens.push( game.add.tween @group
				.to
						x: state.destPoints[i].x
						y: state.destPoints[i].y
					, 500, Phaser.Easing.Sinusoidal.InOut)
			else
				tweens.push(game.add.tween @group
				.to
						x: state.destPoints[i].x
						y: state.destPoints[i].y
					, 500, Phaser.Easing.Sinusoidal.InOut)
				tweens[i-1].chain tweens[i]
			if i is state.destPoints.length-1
				tweens[i].onComplete.add(
					(()->
						_log 'VIEW]попытка завершения промиса'
						deferred.fulfill()

					))
				tweens[0].start()

		return deferred.promise