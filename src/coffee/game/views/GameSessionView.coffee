_log = require('../lib/logger.coffee') 'GameSessionView'
module.exports = class GameSessionView
	constructor: ( )->
		_log 'Старт инициализации'
		@_currentStepLabel=game.add.text(800, 0,"Номер хода 0")
		@_currentStepLabel.font = 'Merriweather'
		@_currentStepLabel.stroke = '#000000';
		@_currentStepLabel.fontSize = 20

	_currentStepLabel: null

	update: ( state )->
		@_currentStepLabel.setText("Текущий ход "+state.currentStep)