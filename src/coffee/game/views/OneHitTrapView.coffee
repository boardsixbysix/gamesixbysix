Q = require 'q'
_log = require('../lib/logger.coffee') 'OneHitTrapView'
module.exports = class OneHitTrapView
	constructor: ( x, y, @_imageName)->
		@group = game.add.group()
		@group.position.set x, y
		@_sprite = game.add.sprite 0, 0, @_imageName, null, @group
		style =
			font: 'Merriweather'
			stroke: '#000000'
			fontSize: 20
		@_label = game.add.text 10, -20, " ", style, @group


	_imageName: null
	_sprite: null
	group:null
	_label: null

	update: ( state )->
		deferred = Q.defer()

		@_label.setText "#{state.name}"

		if state.destPoints.length is 0
			deferred.fulfill()
			return deferred.promise
		tweens = []
		for i in [ 0...state.destPoints.length ]
			if i is 0
				tweens.push( game.add.tween @group
				.to
						x: state.destPoints[i].x
						y: state.destPoints[i].y
					, 500, Phaser.Easing.Sinusoidal.InOut)
			else
				tweens.push(game.add.tween @group
				.to
						x: state.destPoints[i].x
						y: state.destPoints[i].y
					, 500, Phaser.Easing.Sinusoidal.InOut)
				tweens[i-1].chain tweens[i]
			if i is state.destPoints.length-1
				tweens[i].onComplete.add(
					(()=>
						_log 'попытка завершения промиса'
						@group.destroy()
						deferred.fulfill()

					))
				tweens[0].start()

		return deferred.promise
