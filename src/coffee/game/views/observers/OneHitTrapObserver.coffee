OneHitTrapView = require '../OneHitTrapView'

module.exports = class OneHitTrapObserver
	constructor: ( x, y, imageName)->
		@_oneHitTrapUnit = new OneHitTrapView x, y, imageName

	_oneHitTrapUnit:null

	init: ( x, y, imageName )->
		@_oneHitTrapUnit = new OneHitTrapView x, y, imageName
		return @

	update: ( state )->
		@_oneHitTrapUnit.update state
