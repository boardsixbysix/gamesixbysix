#class for updating of the connected views of players
UnitView = require '../UnitView'
UserPanel = require '../UserPanel'
module.exports = class PlayerObserver
	constructor: ( @idModel )->
		@_userPanel = new UserPanel()
	_userPanel: null
	#TODO view для отображения панели игрока в интерфейсе
	#TODO view для отображение характеристик игрока в списке игроков

	init: ( x, y, imageName )->
		@_userPanel = new UserPanel
		return @
		
	getHeightUnit: () ->
		return @_userPanel.getHeight()

	update: ( state )->
		@_userPanel.update state

	setActive: ( flag )->
		@_userPanel.setActive flag