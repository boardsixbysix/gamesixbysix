#class for updating of the connected views of session
GameSessionView = require '../GameSessionView'
module.exports = class SessionObserver
	constructor: ( )->
		@_gameSessionView = new GameSessionView
		
	_gameSessionView: null


	update: ( state )->
		@_gameSessionView.update state