InventoryView = require '../InventoryView'
module.exports = class InventoryObserver
	constructor: (x , y, imageTable)->
		@_inventoryView = new InventoryView x, y, imageTable

	_inventoryView: null

	init: ( x, y, imageTable )->
		@_inventoryView = new InventoryView x, y, imageTable
		return @

	update: ( state )->
		@_inventoryView.update state