#class for updating of the connected views of players
UnitView = require '../UnitView'
module.exports = class UnitObserver
	constructor: ( @idModel )->
	
	_viewUnit:null
	#TODO view для отображения панели игрока в интерфейсе
	#TODO view для отображение характеристик игрока в списке игроков

	init: ( x, y, imageName )->
		@_viewUnit = new UnitView x, y, imageName
		return @
		
	getHeightUnit: () ->
		return @_viewUnit.getHeight()		

	update: ( state )->
		@_viewUnit.update state

	setActive: ( flag )->