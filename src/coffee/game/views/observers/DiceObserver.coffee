DiceView = require '../DiceView'
module.exports = class DiceObserver
	constructor: (x , y, _imageNameUnBlock, _imageNameBlock, action)->
		@_diceView = new DiceView x , y, _imageNameUnBlock, _imageNameBlock, action

	_diceView: null

	update: ( state )->
		@_diceView.update state