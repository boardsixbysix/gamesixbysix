_log = require('../lib/logger.coffee') 'UserPanel'
Q = require 'q'
module.exports = class UserPanel
  constructor: ()->
    @group = game.add.group()
    @group.position.set 37, 530
    style =
      font: 'Merriweather'
      stroke: '#000000'
      fontSize: 30
    @_armor = game.add.text 45, 20, " ARMOR", style, @group
    @_shield = game.add.text 115, 20, " SHIELD", style, @group
    @_movement = game.add.text 185, 20, "MOVEMENT ", style, @group
    @_active = false

  group:null
  _armor: null
  _shield: null
  _movement: null
  _active:null

  update: ( state )->

    if @_active is true
      _log "Update User Panel"
      @_armor.setText "#{state.damage}"
      @_shield.setText "#{state.def}"
      @_movement.setText "#{state.movement}"
    else
      _log "Set Clear View"
      @_armor.setText ""
      @_shield.setText ""
      @_movement.setText ""

  setActive: ( flag )->
    if flag is false
      @_armor.setText ""
      @_shield.setText ""
      @_movement.setText ""
    @_active = flag


