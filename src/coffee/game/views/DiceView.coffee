Q = require 'q'
module.exports = class DiceView
	constructor: (@x ,@y, @_imageNameUnBlock, @_imageNameBlock, @actionOnClick)->
		@_buttonDice = game.add.button @x, @y, @_imageNameUnBlock, @actionOnClick, @, 2, 1, 0
		
	_buttonDice: null

	update: ( state )->
		deferred = Q.defer()
		@_buttonDice.destroy(true)
		if state.blocked
			@_buttonDice = game.add.button @x, @y, @_imageNameBlock, @actionOnClick, @, 2, 1, 0
		else
			@_buttonDice = game.add.button @x, @y, @_imageNameUnBlock, @actionOnClick, @, 2, 1, 0
		deferred.fulfill()

		return deferred.promise