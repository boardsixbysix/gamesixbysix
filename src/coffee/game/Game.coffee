Boot = require './state/Boot'
Preloader = require './state/Preloader'
Level = require './state/Level'
Credits = require './state/Credits'

module.exports = class Game extends Phaser.Game
	@worldMap: null
	constructor: ->
		super 800, 600, Phaser.AUTO, 'content', null

		@state.add 'Boot', Boot
		@state.add 'Preloader', Preloader
		@state.add 'Level', Level
		@state.add 'Credits', Credits

		@state.start 'Boot'


