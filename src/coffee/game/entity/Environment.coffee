Q = require 'q'
_log = require('../lib/logger.coffee') 'Environment'
Promise = require 'bluebird'

initItems = (arr)->
	arr.reduce((promise,trap)->
		promise.then(() ->
			if trap.cleanExecutedRoutines()
				trap.initRoutineFromCell().done((res) ->
					_log ""))
	,Promise.resolve())

module.exports = class Environment
	unitControllers:[]
	trapControllers:[]
	deadUnits: []
	
	addUnitController : ( unitController )->
		@unitControllers.push unitController

	addTrapController : ( trapController )->
		@trapControllers.push trapController


	searchUnitWithId : ( id )->
		for unit in @unitControllers
			if unit.id is id
				return unit

	ExecuteStep: ()->
		#console.log 'Ходит окружение'
		promises = []
		for unit in @_getAggroUnits()
				_log 'Найден сагренный юнит'
				unit.executeCourse()
				unit.updateView()

		for trap in @_getActiveTraps()
				trap.executeCourse()
				
		return Q.all promises

	ExecuteStepTraps: ()->
		promises = []
		for trap in @_getActiveTraps()
			trap.attackExecute()
			trap.updateView()
		return Q.all promises

	initTraps: ()->
		initItems @_getActiveTraps()

	_getActiveTraps: ()->
		return @trapControllers.filter(( unit )-> return unit.aggro() )

	addDeadUnit: (unit)->
		@deadUnits.push unit
		ind = @unitControllers.indexOf unit
		if ind isnt -1
			delete @unitControllers[ind]
			@unitControllers.splice(ind,1)