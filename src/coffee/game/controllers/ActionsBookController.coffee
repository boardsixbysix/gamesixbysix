Controller = require '../controllers/Controller'
ActionsBook = require '../models/ActionsBook'
module.exports = class ActionsBookController extends Controller
  constructor: ( model )->
    super model

  #state -
  # 0 - movement
  # 1 - battle
  getActiveAction: ( state )->
    #Если активный скил для состояния движения
    if state is 0
      return @_model.getActiveMovement()
