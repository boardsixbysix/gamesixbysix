
_log = require('../lib/logger.coffee') 'UnitController'
Navigator = require '../pathing/Navigator'
TargetsControl = require '../Basic/Combat/TargetsControl'
Unit = require '../models/Unit'
Inventory = require '../models/Inventory'
ActionsBook = require '../models/ActionsBook'
ActiveCellsControl = require '../Basic/ActiveCellsControl'
Controller = require '../controllers/Controller'
InventoryController = require '../controllers/InventoriesController'
ActionsBookController = require '../controllers/ActionsBookController'
Promise = require 'bluebird'
{random} = require '../lib/util'

Promise.longStackTraces()
promiseFor = Promise.method((condition, action, value)->
	if !condition(value)
		return value
	return action(value).then(promiseFor.bind(null, condition, action)))

fetchUserDetails = (arr, that)->
	arr.reduce((promise,routine)->
		promise.then(() ->
			routine.execute(  1 + random(6), true ).done((res) ->
				_log ""))
	,Promise.resolve())

Promise.config({
	longStackTraces: true
})

module.exports = class UnitController extends Controller
	constructor: ( model, numberActiveCells, x, y )->
		super( model )
		if numberActiveCells isnt undefined
			@_activeCellsControl = new ActiveCellsControl(numberActiveCells).setUnit x , y , @
		@_routines = []
		@_inventoryController = new InventoryController new Inventory(), @_model
		@_actionsBookController = new ActionsBookController( new ActionsBook @ )

	_combatRoutine: null
	_activeCellsControl: null
	_inventoryController: null
	_routines: null
	_actionsBookController: null

	getCombatRoutine: ()->
		@_combatRoutine

	decreaseHpFromFirstTarget: ( damage )->
		@_model.getTarget
		@_targetsControlService.getFirstTarget().decreaseHp diceRoll
		_log @_targetsControlService.getFirstTarget()
		return @_targetsControlService.getFirstTarget().updateView()

	setCombatRoutine: (@_combatRoutine)->
		@_combatRoutine.setUnit @_model, @_targetsControlService, @_activeCellsControl

	addTarget: ( unit )->
		@_targetsControlService.addTarget(unit)
		_log "Добавление юнита в цели"
		_log @_targetsControlService
		return

	getTarget: () ->
		@_targetsControlService.getFirstTarget()
		
	aggro: ()->
		_log 'моб сагрен? ' + !@_targetsControlService.isEmptyTargets()
		!@_targetsControlService.isEmptyTargets()

	resetAggro: ()->
		_log 'Сброс агра для игрока ' + name
		@_targetsControlService.resetTargets()
		return

	isDead: ()->
		_log 'Юнит мертв? ' + @_model.isDead()
		return @_model.isDead()
	
	decreaseHp: ( damage )->
		_log 'вызвано уменьшения ХП ' + damage
		@_model.decreaseHp damage - @getArmor()
		if @_model.isDead()
			@_combatRoutine.death()

	executeCourse: (  )->
		_log "Обновление ловушки"
		@_combatRoutine.attackMovement @_model.damage
		console.log "Обновление вью"
		@updateView()
		.then =>
			_log "Выполнить атаку"
			_log @_combatRoutine
			@_combatRoutine.attackExecute @_model.damage
			_log "Обновление вью"
			@updateView()
		.done()

	initRoutineFromCell: ()->
		if @_routines.length == 0
			_log "Сценариев на клетки #{@_model.currentCell.getRoutines().length}"
			@_routines = @_model.currentCell.getRoutines()
			_log "клонирование завершено"
			_log @_routines
			initPromise = @_routines.filter((p)-> p.isExecuteOnInit())
			that = @
			fetchUserDetails(initPromise, that)
		return

	executeRoutine: ( diceRoll )->
		_log "Выполнение броска. Проверка на агр #{@aggro()}"
		if @aggro()
			_log "Выполнение действия при агре"
			return @_actionsBookController.getActiveAction( 1 ).use diceRoll
		else
			_log "Выполнение действия при движении"
			return @_actionsBookController.getActiveAction( 0 ).use diceRoll

	getLocation: ()->
		_log @_model.currentCell
		return @_model.currentCell
		
	getBaseAttack: ()->
		@_model.getBaseAttack()

	getInventoryController: ()->
		@_inventoryController

	getMovement:()->
		_log "скорость равна "
		_log @_model.speed
		_log @_model.speed + @_inventoryController.getMovementBonus()
		@_model.speed + @_inventoryController.getMovementBonus()

	getMaxHp:()->
		@_model.maxHp

	getHp:()->
		@_model.hp

	getArmor:()->
		@_model.def

	getAttack:()->
		@_model.getBaseAttack()

	getAgility:()->
		@_model.agility


