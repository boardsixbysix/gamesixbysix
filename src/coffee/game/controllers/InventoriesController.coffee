_log = require('../lib/logger.coffee') 'InventoriesController'
Inventory = require '../models/Inventory'
Controller = require '../controllers/Controller'
#Table of inventory. To show after press button "B"
module.exports = class InventoriesController extends Controller
	constructor: ()->
		super( [] )

	get: ( idPlayer )->
		_log @_model
		_log idPlayer
		for inventory in @_model
			if inventory.id is idPlayer
				_log inventory
				return inventory

	put: ( inventory )->
		@_model.push inventory
						
	#Action on press button "Clear" at inventory display
	clearAll:() ->
		#@_model.clearAll()

	#Action on drag and drop item
	addItem:( item ) ->

		#Обработать каждый предмет
		@_model.addItem item

	addItems:( items ) ->
		for item in items
			@_model.addItem item

					
	#Action on drag and drop item
	deleteItem:() ->
		#TODO

	#Action on press button "B"
	show:() ->
		#TODO

	#Action on press button "B"
	hide:() ->
		#TODO

	useItem: ( invId, itemId, unit )->
		for item in @_model.getItems()
			if item.id == itemId
				item.useOn( @unit )

	getMaxHpBonus:()->
		maxHp = 0
		for item in @_model.getItems()
			if item.isPut
				maxHp += item.itemStats.maxHpBonus
		return maxHp

	getHpBonus:()->
		Hp = 0
		for item in @_model.getItems()
			if item.isPut
				Hp += item.itemStats.HpBonus
		return Hp

	getMovementBonus:()->
		movement = 0
		_log 'Total bag'
		for item in @_model.getItems()
			if item.isPut
				movement += item.itemStats.movement
		return movement

	getArmorBonus:()->
		Armor = 0
		for item in @_model.getItems()
			if item.isPut
				Armor += item.itemStats.armor
		return Armor

	getAttackBonus:()->
		Attack = 0
		for item in @_model.getItems()
			if item.isPut
				Attack += item.itemStats.attack
		return Attack

	getShieldBonus:()->
		Attack = 0
		for item in @_model.getItems()
			if item.isPut
				Attack += item.itemStats.attack
		return Attack
