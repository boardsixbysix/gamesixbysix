_log = require('../lib/logger.coffee') 'DiceController'
Dice = require '../models/Dice'
DiceObserver = require '../views/observers/DiceObserver'
Controller = require '../controllers/Controller'
{random} = require '../lib/util'
module.exports = class DiceController extends Controller
	constructor: ()->
		super(new Dice)

	_countThrow: 0
		
	throwDice : (  )->
		number = null
		blocked = null
		if @_model.getState().blocked
			return

		_log "скорость перса равна "
		number =  1 + random 6
		@_countThrow--;
		if @_countThrow is 0
			blocked = true
		@_model.setState number, blocked
		return @updateView()
			
	switchDiceOn: ( currentPlayer )->
		#TODO check buffs player
		@_model.unBlock()
		@_model.setDefaultCountOfThrow()
		
	getDiceRoll: ()->
		return @_model.getState().diceRoll

	executeRoutine: ()->
		@_model.currentCell.executeRoutine( @getDiceRoll() )


