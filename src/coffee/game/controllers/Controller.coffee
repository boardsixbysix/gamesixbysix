Q = require 'q'
_log = require('../lib/logger.coffee') 'Controller'
module.exports = class Controller
	constructor: (@_model)->
		@_observers = []

	_model: null
	_observers: null

	get: ()->
		@_model

	getObservers: ()->
		return @_observers

	attach: ( observer )->
		@_observers.push observer

	updateView: ()->
		promises = []
		for observer in @_observers
				promises.push observer.update(@_model.getState( ))
		return Q.all promises

	updateViews: ()->
		promises = []
		for model in @_model
			for observer in @_observers
				promises.push observer.update(model.getState( ))
			return Q.all promises


