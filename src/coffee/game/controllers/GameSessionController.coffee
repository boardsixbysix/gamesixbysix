_log = require('../lib/logger.coffee') 'GameSessionController'
Controller = require '../controllers/Controller'
GameSession = require '../models/GameSession'
module.exports = class GameSessionController extends Controller
  constructor: ( )->
    super( new GameSession() )

  addPlayer: ( player )->
    @_model.add player

  startProcess: ()->
    @_model.setCurrentFirstPlayer()
    game.playersController.setCurrentStep @_model.current.id
    
  getCurrentPlayer: ()->
    return @_model.current
    
  getState: ()->
    @_model.getState()

