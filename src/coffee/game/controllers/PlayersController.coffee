TargetsControl = require '../Basic/Combat/TargetsControl'
_log = require('../lib/logger.coffee') 'PlayersController'
UnitController = require '../controllers/UnitController'
Unit = require '../models/Unit'
{ init } = require '../../../assets/maps/testmap/actions/0'
RelationCell = require '../Basic/RelationCell'
PlayerObserver = require '../views/observers/PlayerObserver'
UnitObserver = require '../views/observers/UnitObserver'
Q = require 'q'

module.exports = class PlayersController extends UnitController
	constructor: ( )->
		super []

	get: ( id )->
		for player in @_model
			if player.id is id
				return player

	put: ( unitModel )->
		@_model.push unitModel
		@attach new PlayerObserver unitModel.id
		@attach new UnitObserver( unitModel.id ).init game.worldMap.mapPath[0].x, game.worldMap.mapPath[0].y, 'player'

	executeAttack: ( dice_roll, init ) ->
		@_combatRoutine.attackExecute dice_roll, init
		
	canThrow: ()->
		return @_model.isAlive()	
		
	executeCourse: ( diceRoll )->
		promise = null
		_log "Запуск хода"
		if @aggro()
			_log "Игрок сагрен. Выполнение атаки"
			_log promise
			return @_executeAttack diceRoll
		#_log 'Вызов команды передвижения!'
		else
			_log 'Вызов передвижения'
			_log diceRoll
			_log @_combatRoutine
			@_combatRoutine.attackMovement diceRoll
			_log 'Вызов обновления вью через бросок кубика'
			promise = @updateView()
		return promise
							
	decreaseHp: ( damage )->
		_log 'вызвано уменьшения ХП ' + damage
		@_model.decreaseHp damage - @getArmor()
		if @_model.isDead()
			game.resurrectionService.addPlayer( @ )

	setCurrentStep: ( modelId )->
		_log "SetActive COntroller"
		for observer in @_observers
			observer.setActive true
		return

	clearState: ()->
		for observer in @_observers
			observer.setActive false
		return

	changeCellEvent:( oldCell, newCell, modelId )->
		if modelId is undefined
			for player in @_model
				#TODO Поиск возможного сценария в ресурсах по id клетки и выполнение его инициализации
				if newCell.id is 0
					init @get( player.id )
					@get( player.id ).currentCell = newCell
			_log 'привязка к игрока к новой клетке'
		else
			@get( modelId ).currentCell = newCell
		#Выполнение деинициализации предыдущего сценария

	executeAction: ( cell, idModel )->
		#Выполнение сценарий
