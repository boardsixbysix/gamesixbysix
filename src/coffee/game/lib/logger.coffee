dummy = ->
info = console.info.bind console
warn = console.warn.bind console
error = console.error.bind console

all = console.log.bind console
all.info = info
all.warn = warn
all.error = error

infos = ->
infos.info = info
infos.warn = warn
infos.error = error

warns = ->
warns.info = dummy
warns.warn = warn
warns.error = error

errors = ->
errors.info = dummy
errors.warn = dummy
errors.error = error

none = ->
none.info = dummy
none.warn = dummy
none.error = dummy


config =
	default: none
	start: none
	MovementLocationRoutine: all
	UnitController: all
	Point : all
	AggroLocationRoutine : all
	PlayerCombatRoutineStory : all
	UnitController : all
	PlayerController : all
	MovementAbility : all
	ActionsBook : all
	PlayersController : all
	InventoriesController : all
	Level : all

module.exports = logger = ( cat, args... )->
	log = config[cat] ? config.default
	if prefix?
		bound = log.bind console.log, args...
		bound.info = log.info.bind console.log, args...
		bound.warn = log.warn.bind console.log, args...
		bound.error = log.error.bind console.log, args...
		log = bound
	log
