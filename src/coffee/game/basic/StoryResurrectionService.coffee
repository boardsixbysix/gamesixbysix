_log = require('../lib/logger.coffee') 'StoryResurrectionService'

module.exports = class StoryResurrectionService

	_recoveryPlayers: []

	update: ()->

	updateForPlayers: ()->

		for recPlayer in @_recoveryPlayers
			if recPlayer.isReadyForRecovery
				recPlayer.player.recovery()
				ind = @_recoveryPlayers.indexOf recPlayer
				delete @_recoveryPlayers[ind]
				@_recoveryPlayers.splice ind, 1
				_log 'Удаление игрока из массива восстановления'
				_log @_recoveryPlayers
			else
				_log 'готовность к удалению'
				recPlayer.isReadyForRecovery = true

	addPlayer: ( player )->
			_log "Проверка на смерть"
			_log player.isDead()
			if player.isDead() 
				for recPlayer in @_recoveryPlayers
					if recPlayer.player.id is player.id
						@_recoveryPlayers.push
							player: player
							isReadyForRecovery: false		
			return
	isResurrectionEveryone: ()->
		return @_recoveryPlayers.length is game.gameSessionController.getState().count

