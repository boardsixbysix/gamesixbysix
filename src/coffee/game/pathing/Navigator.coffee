_log = require('../lib/logger.coffee') 'Navigator'
WorldMap = require '../pathing/WorldMap'

module.exports = class Navigator

	constructor: ()->

	@layRoute: ( number, source ) ->
		destPoints = []
		number = Navigator._performDestinationNumber(source + number, source)
		_log number
		destNumber = number
		point1 = game.worldMap.mapPath[number]
		destPoints.push point1
		if point1.pointType is 4
			destNumber = number + point1.extra
			point2 = game.worldMap.mapPath[destNumber]
			destPoints.push point2

		source = destNumber
		_log "Расчет точек"
		return destPoints

	@_performDestinationNumber: ( number, source )->
		for i in [ source + 1..number ]
			if game.worldMap.mapPath[i].isStopCell()
				return i
		return number


	@performNearlyCells: ( x,y,count )->

		number=0
		mapCopy = ( cell for id, cell of game.worldMap.mapPath )

		#_log 'Координаты начальной точки'+game.worldMap.mapPath[0].x+' '+game.worldMap.mapPath[0].y
		#_log 'Координаты монстра '+x+' '+y
		minDist= Phaser.Math.distance( game.worldMap.mapPath[0].x, game.worldMap.mapPath[0].y, x, y)
		#_log 'Начальная дистанция '+minDist

		if count is 1
			for i in [1..game.worldMap.mapPath.length-1]
				if Phaser.Math.distance(x, y, game.worldMap.mapPath[i].x, game.worldMap.mapPath[i].y) < minDist
					minDist = Phaser.Math.distance(x, y, game.worldMap.mapPath[i].x, game.worldMap.mapPath[i].y)
					number = i
			return number

		mapCopy.sort( ( a, b )->
			if Phaser.Math.distance( a.x, a.y, x, y) < Phaser.Math.distance( b.x, b.y, x, y)
				return -1
			if Phaser.Math.distance( a.x, a.y, x, y) > Phaser.Math.distance( b.x, b.y, x, y)
				return 1
			return 0
		)
		#_log 'Подходящие точки '+mapCopy.slice( 0,count )
		return mapCopy.slice( 0,count )

