Point = require '../models/Cell'
{random} = require '../lib/util'

#
# WIP
_p = ( x, y )-> { x, y }
_createPath = ->
	points = []
	points.push _p 0, 0
	for x in [ 80..720] by 70
		points.push _p x, 50
	for y in [ 120..200 ] by 70
		points.push _p 720, y
	for x in [ 700..0 ] by -70
		points.push _p x, 260
	for y in [ 330..400 ] by 70
		points.push _p 20, y
	for x in [ 90..500 ] by 70
		points.push _p x, 400
	points.push _p 520, 400
	points

_randomPop = ( arr )->
	[ el ] = arr.splice random(arr.length - 1), 1
	el

_createMap = ( game )->
	cells = {}
	path = _createPath()


	points = [ 0...path.length ]
	points.shift()
	points.pop()

	types = ( 0 for [ 0...path.length ] )
	types[0] = 1
	types[types.length - 1] = 2

	#TODO: add mob aggregation here, before special cells
	#TODO: invalid fly cells check: out of bounds and cycles

	for [ 0..1 ]
		types[_randomPop points] = 3
	for [ 0..3 ]
		types[_randomPop points] = 4

	flight_distances = [ 7, -7, 3, -3 ]

	for id, pos of path
		cells[id] = switch types[id]
			when 0
				Point.createSimpleCell game, pos.x, pos.y
			when 1
				Point.createStartMapCell game, pos.x, pos.y
			when 2
				Point.createFinishCell game, pos.x, pos.y
			when 3
				Point.createStopCell game, pos.x, pos.y
			when 4
				Point.createFlyCell game, pos.x, pos.y, flight_distances.shift()
			else
				console.error 'unknown cell type'

	cells

module.exports = class WorldMap
	mapPath: null

	constructor: ( game )->
		@mapPath ?= _createMap game


