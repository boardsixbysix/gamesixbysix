Q = require 'q'
_log = require('../../lib/logger.coffee') 'MovementLocationRoutine'
module.exports = class MovementLocationRoutine
  constructor: (@cell) ->
    @priority = 10
    @id = 0
    @reUse = true
  cell: null
  priority:null
  isMoved:null
  executed:null
  reUse:null
  isExecuteOnInit: ()->
    false

  isComplete: () ->
    @executed

  setCell: (@cell)->

  execute: (  diceRoll, playerController)->
    _log 'Вызов стратегии передвижения'
    if @executed
      _log 'Передвижение уже было совершено'
      return null
    else
      _log 'Передвижение инициируется'
      @executed = true
      return 	playerController.movement diceRoll

  resetUsable: ()->
    @executed = false

  clone:()->
    movementLocationRoutine = new MovementLocationRoutine @cell
    movementLocationRoutine.executed = @executed
    return movementLocationRoutine