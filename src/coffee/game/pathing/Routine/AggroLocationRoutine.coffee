_log = require('../../lib/logger.coffee') 'AggroLocationRoutine'
Q = require 'q'
module.exports = class AggroLocationRoutine
	
	constructor: (@cell) ->
		@priority = 2
		@id = 1
		@aggro = false
		@completed = false
		@executeOnInit = true
		@reUse = true
	cell: null
	aggro:null
	priority:null
	completed:null
	executeOnInit: null
	reUse:null

	isComplete: () ->
		@completed

	isExecuteOnInit: ()->
		true

	setCell: (@cell)->
		@aggro = false

	execute: ( diceRoll, init)->
		_log 'Выполнение агро стратегии клетки'
		_log init
		if @aggro isnt true
			_log 'Моб еще не сагрен. Добавление агра'
			game.session.current.addTarget @cell.aggroUnitController
			@cell.aggroUnitController.addTarget(game.session.current)
			_log game.session.current

			@aggro = true
		else
			if game.session.current.getTargetControls().getFirstTarget().isDead()
				@completed = true
		if init && game.session.current.getAgility() > game.session.current.getTargetControls.getFirstTarget().getAgility()
			return game.session.current.getAbility().use diceRoll
		_log 'Выполнение атаки'
		return game.session.current.executeAttack( diceRoll, init )

	resetUsable: ()->
		@completed = false

	clone:()->
			aggroLocationRoutine = new AggroLocationRoutine @cell
			aggroLocationRoutine.executed = @executed
			aggroLocationRoutine.aggro = @aggro
			return aggroLocationRoutine