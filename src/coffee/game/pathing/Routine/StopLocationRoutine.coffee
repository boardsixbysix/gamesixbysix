Q = require 'q'
module.exports = class StopLocationRoutine
  constructor: (@cell) ->

  cell: null
  priority:null
  setCell: (@cell)->

  execute: (  diceRoll, playerController )->
    console.log 'Текущая клетка является финишной'
    game.session.addFinishCurrentPlayer()
    Q()
