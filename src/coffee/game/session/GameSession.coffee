_log = require('../lib/logger.coffee') 'GameSession'

SessionObserver = require '../views/observers/SessionObserver'
storyResurrectionService = require '../basic/StoryResurrectionService'

module.exports = class GameSession
	constructor: ( @game)->
		@userControllers = []
		@finishSession = []
		@loseSession = []

	sessionObserver:null
	userControllers: null
	game:null
	currentStep:0
	current:null
	currentNumberPlayer:0
	state:null
	finishSession:null
	loseSession:null
	countPlayers: 0
	gameOver:false

	getState: ()->
		currentStep: @currentStep
	
	getCountPlayer: ()->
		return @userControllers.length
	
	isFinishEveryone: ()->
		return @finishSession.length is @countPlayers
		
	addFinishCurrentPlayer: ()->
		@_addFinishPlayer @currentNumberPlayer

	addPlayerController: ( userController )->
		@userControllers.push(userController)

	isLastStep: () ->
		_log.info "Последний ход?"
		_log.info @currentNumberPlayer is @userControllers.length-1
		return  @currentNumberPlayer is @userControllers.length-1

	startProcess: ()->
		_log 'Старт карты'
		@sessionObserver = new SessionObserver @
		@current=@userControllers[0]
		@current.setCurrentStep()
		@sessionObserver.update(@getState())
		@countPlayers = @userControllers.length
		_log @userControllers
		for controller in @userControllers
			_log 'Вызов вью при старте карты для контроллера '
			#Привязка контроллеров игроков к стартовой клетке
			controller.changeCellEvent null, game.worldMap.mapPath[0], 0
			#controller.initRoutineFromCell()
			controller.updateView()

		game.diceController.switchDiceOn @userControllers[@currentNumberPlayer]
		game.diceController.updateView()

	switchPlayer: ()->
		_log 'Вызов переключения игроков'
		if @isFinishEveryone()
			return
		@userControllers[@currentNumberPlayer].clearState()
		if @currentNumberPlayer is @userControllers.length-1
			@_handleLastStep()		
		else
			@currentNumberPlayer++;
		@current = @userControllers[@currentNumberPlayer]
		@current.setCurrentStep()
		_log @current
		if !@current.canThrow()
			_log 'Текущий игрок не может ходить'
			@switchPlayer()
		_log 'Вызов обновления вью через переключения игроков'
		@sessionObserver.update @getState()
		game.diceController.switchDiceOn @userControllers[@currentNumberPlayer]
		game.diceController.updateView()

	_handleLastStep: ()->

		@currentNumberPlayer=0;
		@currentStep++;

	_addFinishPlayer: ( id )->
		@userControllers[id].state = "Finish"
		@finishSession.push @userControllers[id]
		delete @userControllers[id]
		@userControllers.splice(id,1)


				
		