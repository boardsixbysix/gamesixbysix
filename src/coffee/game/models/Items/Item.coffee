_log = require('../../lib/logger.coffee') 'Item'
GameModel = require '../../entity/GameModel'
ItemStats = require '../ItemStats'
module.exports = class Item extends GameModel
	constructor: ( @id, @name, @typeItem, @itemStats )->
		@_isPutOn = false
		@effects = []
		
	effects: null
		
	
	getState: ()->
		name: @name
		id: @id
		typeItem: @typeItem
		itemStats: @itemStats

	typeItem:null
	#Флаг, показывающий одетость предмета
	_isPutOn:null

	useOn: ( unit )->
		@_isPutOn = true
		unit.hp += @itemStats.maxHpBonus
		unit.maxHp += @itemStats.maxHpBonus
		unit.damage += @itemStats.attack
		unit.movement += @itemStats.movement
		unit.def += @itemStats.armor

	putOff: ()->
		@_isPutOn = true

	isPut: ()->
		return @_isPutOn
