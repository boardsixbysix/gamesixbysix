_log = require('../lib/logger.coffee') 'Unit'
Q = require 'q'
GameModel = require '../entity/GameModel'
Inventory = require '../models/Inventory'


class Unit extends GameModel
	constructor: ( @id, @name,  @maxHp, @def, @damage, @level, @type,speed, @agility)->
		@hp = @maxHp
		@speed = speed
		_log @speed
		_log speed
		@_destPoints = []
		_log "Скорость равна"

	_attitude: null
	_destPoints: null
	currentCell: null
	#Скорость атаки
	agility: null
	movement: null
	_targetEnemy : null

	getState: (  )->
			name: @name
			hp: @hp
			destPoints: @_destPoints
			isDead: @isDead()
			def : @def
			damage : @damage
			targetEnemy : @_targetEnemy
			aggro : @isAggro()

	setDestPoints: ( _destPoints )->
		_log "Установка маршрута"
		@_destPoints = _destPoints

	isImpegnable: () ->
		return @hp is -1

	isAlive: () ->
		return @hp > 0

	isAggro:() ->
		@_targetEnemy isnt null

	setTarget:( target ) ->
		@_targetEnemy = target

	resetTarget: ()->
		@_targetEnemy = null

	getTarget: ()->
		@_targetEnemy

	isDead: () ->
		_log "Проверка на смерть для юнита "+@name+@hp
		return @hp <= 0
		
	getBaseAttack: ()->
		return @damage

	recovery: ( )->
		_log 'Активация перерождения!'+@hp
		@hp = @maxHp
		@updateView()
		return
		
	decreaseHp: ( damage )->
		@hp = @hp - damage
		_log 'атака на '+damage+' Осталось '+@hp
		if @hp <= 0
			@hp = 0
		return

exports.createAggressive = ( id, name,  maxHp, def, damage, level, speed, agility)->
	unit = new Unit id, name,  maxHp, def, damage, level, 0, speed, agility

	unit._attitude = "Aggressive"
	return unit

exports.createStoryPlayer = ( id, name,  maxHp, def, damage, level, speed, agility)->
	unit = new Unit id, name,  maxHp, def, damage, level, 1, speed, agility
	_log unit
	unit._attitude = "Neutral"

	return unit

exports.createTrap = ( id, name,  maxHp, def, damage, level, speed,attack, agility)->
	unit = new Unit id, name,  maxHp, def, damage, level, 2, speed,attack, agility
	unit._attitude = "Neutral"
	return unit



	