_log = require('../lib/logger.coffee') 'Inventory'
GameModel = require '../entity/GameModel'

module.exports = class Inventory extends GameModel
	constructor: ( @id )->
		@_isVisible = false;
		@_items = []
						
	_items: null
	_isVisible:null

	switchVisible: () ->
		@_isVisible = !@_isVisible

	clearAll: () ->
		@_items = []

	#Action on drag and drop item
	addItem:( item ) ->
		@_items.push item
	

	addItems:( items ) ->
		for item in items
			@_items.push item

	getItems: ()->
		return @_items

	getItem: ( id )->
		for item in @_items
			if item.id == id
				return item
				
	#Return state of model for View
	getState: () ->
		items: @_items
		
	useItemOn: ( unit, itemId)->
		@getItem( itemId ).useOn( unit )

exports.createInventory = ( items ) ->
		return new Inventory(items)

