Ability = require '../Ability'
_log = require('../../lib/logger.coffee') 'MovementAbility'
module.exports = class MovementAbility extends Ability
  constructor: (unitController) ->
    super unitController, 0, "Movement Ability", "Forward"

  use: ( diceRoll )->
    _log "Выполнение способности Передвижение"
    #_log "Старт движения"
    @unitController.getModel().setDestPoints []
    #_log "Вызов прощета марщрута"
    #_log.info @damage
    #_log.info @unit.currentCell.id
    points = Navigator.layRoute diceRoll, @unitController.getModel().currentCell.id
    #_log.info "завершен расчет пути"
    #_log.info points
    @unitController.getModel().currentCell = points[points.length-1]
    @unitController.getModel().setDestPoints points
    #_log.info  "Заверешнеи вычисления маршрута"