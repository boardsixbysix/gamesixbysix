Ability = require '../Ability'
_log = require('../../lib/logger.coffee') 'SimpleAttackAbility'
module.exports = class SimpleAttackAbility extends Ability
	constructor: (unitController) ->
		super unitController, 0, "Movement Ability", "Forward"

	use: ( diceRoll )->
		return @unitController.decreaseHpFromFirstTarget diceRoll