_log = require('../lib/logger.coffee') 'Cell'
GameObject = require '../entity/GameModel'
AggroLocationRoutine = require '../pathing/Routine/AggroLocationRoutine'
FinishLocationRoutine = require '../pathing/Routine/FinishLocationRoutine'
MovementLocationRoutine = require '../pathing/Routine/MovementLocationRoutine'

class Cell extends GameObject
	constructor: (@game)->
		Cell.counter++
		@id = Cell.counter
		@_locationRoutines = []
		@relationsNpcController = []
		@relationsPlayerController = []
		@_addLocationRoutine new MovementLocationRoutine @

	@counter:-1
	pointType: 0
	kids: []
	extra: null
	effect: null
	sprite: null
	spriteName:null
	x: 0
	y: 0

	_actions: null

	addRelationPlayerController: ( relation )->
		@relationsPlayerController.push relation

	_addLocationRoutine: ( routine ) ->
		@_locationRoutines.push routine
		@_locationRoutines.sort( ( a, b )->
			if a.priority < b.priority
				return -1
			if a.priority > b.priority
				return 1
			return 0
		_log "Добавление завершено. Итоговый список: "
		_log @_locationRoutines
		#_log @getRoutines()
		)

	getWidth: ()->
		return @sprite.width

	getHeight: ()->
		return @sprite.height
	
	isStopCell: ()->
		return @pointType is 1 || @pointType is 5
	
	isAggro: ()->
		@aggroUnitController?
		
	isFinish: ()->
		return @pointType is 5
		
	resetAggro: ()->
		@aggroUnitController = null
		@_locationRoutines = null
		@sprite.kill()
		@sprite = game.add.sprite @x, @y, @spriteName
		
	setPreAggroEffect: ()->
		@sprite = game.add.sprite @x, @y, 'preAggroCell'

	getRoutines: ()->
		routins = @_locationRoutines.filter(( routine )-> return !routine.isComplete() && !routine.reUse )
		_log "Всего сценариев: #{@routins.length}"
		_log @id
		_reuseLocationRoutine = @_locationRoutines.filter(( routine )-> return routine.reUse )
		for routine in _reuseLocationRoutine
			_log "Клонирование объекта"
			_log routine
			cloneRoutine =  routine.clone()
			cloneRoutine.resetUsable()
			routins.push cloneRoutine
		return routins

	addAggroRoutine: ()->
		_log "Добавления агро сценария"+@id

		@_addLocationRoutine new AggroLocationRoutine( @ )

exports.createSimpleCell = ( game, x, y )->
	point = new Cell(game)
	point.spriteName = 'cell'
	point.sprite = game.add.sprite x, y, 'cell'
	point.x = x
	point.y = y
	point.pointType = 0
	point

exports.createStartMapCell = ( game, x, y )->
	point = new Cell(game)
	point.spriteName = 'startCell'
	point.sprite = game.add.sprite x, y, 'startCell'
	point.x = x
	point.y = y
	point.pointType = 3
	point

exports.createStopCell = ( game, x, y )->
	point = new Cell(game)
	point.spriteName = 'stopCell'
	point.sprite = game.add.sprite x, y, 'stopCell'
	point.x = x
	point.y = y
	point.pointType = 1
	point

exports.createFinishCell = ( game, x, y )->
	point = new Cell(game)
	point.spriteName = 'finishCell'
	point.sprite = game.add.sprite x, y, 'finishCell'
	point.x = x
	point.y = y
	point.pointType = 5
	#point._locationRoutine.push FinishLocationRoutine point
	point	
	
exports.createFlyCell = ( game, x, y, steps )->
	point = new Cell(game)
	point.spriteName = 'cell'
	point.sprite = game.add.sprite x, y, 'cell'
	point.extra = steps
	label = game.add.text x+14, y+14, steps>0 and "+#{steps}" or steps
	label.font = 'Merriweather'
	label.stroke = '#000000';
	label.fontSize = 28
	point.x = x
	point.y = y
	point.pointType = 4
	#point._reuseLocationRoutine.push new AggroLocationRoutine point
	point

