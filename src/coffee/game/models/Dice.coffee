_log = require('../lib/logger.coffee') 'Dice'
GameObject = require '../entity/GameModel'
{random} = require '../lib/util'
module.exports = class Dice extends GameObject
	constructor:()->

	_diceRoll: null
	_blocked: null
	_countThrow: 0

	getState: ()->
		diceRoll: @_diceRoll
		blocked: @_blocked
		
	setState: ( @_diceRoll, @_countThrow )->
		
	unBlock: ()->
		@_blocked = false

	block: ()->
		@_blocked = true

	setDefaultCountOfThrow: () ->
		@_countThrow = 1
		
###	updateView: ()->
		_log "Dice: updateView"
		_log @_observers
		if @_observers.length is 1
			return @_observers[0].update(@getState())
		promise = null
		for observer in @_observers
			if promise is null
				promise = observer.update(@getState())
			else
				promise.then observer.update(@getState())
		return promise###
