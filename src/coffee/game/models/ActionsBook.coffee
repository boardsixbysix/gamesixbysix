GameObject = require '../entity/GameModel'
MovementAbility = require '../models/Abilities/MovementAbility'
SimpleAttackAbility = require '../models/Abilities/SimpleAttackAbility'
_log = require('../lib/logger.coffee') 'ActionsBook'
module.exports = class ActionsBook extends GameObject
  constructor:( @unitController ) ->
    @actions = []
    @actions.push new MovementAbility @unitController
    @actions.push new SimpleAttackAbility @unitController
    _log "Инициализация книги действий. Всего в книге #{@actions.length}"
    @trySwitchActiveMovementSkillOn 0

  _activeMovement: null
  _activeBattle: null

  trySwitchActiveMovementSkillOn: ( skillId )->
    _log "Попытка переколючения активного скила на бег"
    for action in @actions
      _log action
      if action.id is skillId && action.available
        _log "Найден доступный скил на бег"
        @_activeMovement = action
        return true
    return false

  trySwitchActiveCombatSkillOn: ( skillId )->
    _log "Попытка переколючения активного скила на бег"
    for action in @actions
      _log action
      if action.id is skillId && action.available
        _log "Найден доступный скил на бег"
        @_activeBattle = action
        return true
    return false

  getActiveMovement: ()->
    return @_activeMovement

  getActiveCombat: ()->
    return @_activeMovement