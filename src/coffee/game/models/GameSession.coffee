_log = require('../lib/logger.coffee') 'gameSession'
GameObject = require '../entity/GameModel'
module.exports = class GameSession
  constructor: ()->
   @_players = []

  _players: null
  currentStep:0
  current:null
  finishSession:null
  loseSession:null
  gameOver:false
  currentNumberPlayer:0

  getState: ()->
    currentStep: @currentStep
    current : @current
    count: @_players.length
    
  add: ( player )->
    @_players.push player

  getCountPlayer: ()->
    @_players.length
  
  setCurrentFirstPlayer: ()->
    @current = @_players[0]