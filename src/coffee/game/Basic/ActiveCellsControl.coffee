WorldMap = require '../pathing/WorldMap'
Navigator = require '../pathing/Navigator'
_log = require('../lib/logger.coffee') 'ActiveCellsControl'
AggroLocationRoutine = require '../pathing/Routine/AggroLocationRoutine'

module.exports = class ActiveCellsControl
	
	constructor: ( @countAggroCells ) ->


	_aggroCells: null
	_countAggroCells:null
	_unitController:null

	setUnit: ( x, y,@_unitController  )->
		@_aggroCells = Navigator.performNearlyCells x, y, @countAggroCells
		@_setAggro()
		return @

	#numbers is massive
	setActiveCells: ( numbers )->
		for nCell in numbers
			@_aggroCells.push game.worldMap.mapPath[nCell]

	_setAggro: ()->
		for cell in @_aggroCells
			cell.aggroUnitController = @_unitController
			cell.setPreAggroEffect()
			cell.addAggroRoutine()
		return

	resetAggroCells: ()->
		cell.resetAggro() for cell in @_aggroCells
		@_aggroCells.length = 0
		return

	_performNearlyGoal: ()->

		