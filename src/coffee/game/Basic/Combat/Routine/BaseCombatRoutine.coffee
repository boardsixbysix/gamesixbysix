_log = require('../../../lib/logger.coffee') 'BaseCombatRoutine'
module.exports = class BaseCombatRoutine

	unit:null
	targetsControl: null
	
	setUnit: (@unit, @targetsControl, @activeCellsControl)->

	
	attackExecute: ( @damage, init )->

	attackMovement: ( @damage )->
		_log "Вызов базового метода"
		

	basePvEAttack: ()->
	
	baseAOEAttack: ()->

	death: ()->
		if @activeCellsControl isnt null
			@activeCellsControl.resetAggroCells()
		for plAggro in @targetsControl.getAllTargets()
			if plAggro.aggro()
				plAggro.resetAggro()
		game.environment.addDeadUnit(@unit)
		@targetsControl.resetTargets()

