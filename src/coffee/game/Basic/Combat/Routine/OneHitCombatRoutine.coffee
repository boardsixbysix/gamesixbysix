_log = require('../../../lib/logger.coffee') 'OneHitCombatRoutine'
BaseCombatRoutine = require './BaseCombatRoutine'
Navigator = require '../../../pathing/Navigator'
Q = require 'q'
module.exports = class OneHitCombatRoutine extends BaseCombatRoutine

	constructor: ()->

	attackMovement: ( cells )->
		@unit.setDestPoints []
		_log "Точка назначения"
		_log @targetsControl.getFirstTarget().getLocation().id
		points = Navigator.layRoute 0,@targetsControl.getFirstTarget().getLocation().id
		@unit.setDestPoints points
		return Q()

	attackExecute: ( @damage)->
		@targetsControl.getFirstTarget().decreaseHp @unit.getBaseAttack()
		@death()

