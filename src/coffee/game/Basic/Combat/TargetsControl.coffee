_log = require('../../lib/logger.coffee') 'TargetsControl'
module.exports = class TargetsControl
	
	constructor: ()->
		@_aggroPlayers = []
	
	addTarget: ( player )->
		@_aggroPlayers.push player

	getFirstTarget: ()->
		_log @_aggroPlayers
		return @_aggroPlayers[0]

	getAllTargets: ()->
		return @_aggroPlayers

	isEmptyTargets: ()->
		return @_aggroPlayers.length is 0
		
	resetTargets: ()->
		@_aggroPlayers = []
		return