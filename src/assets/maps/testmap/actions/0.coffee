ItemStats = require '../../../../coffee/game/models/ItemStats'
Item = require '../../../../coffee/game/models/Items/Item'
{random} = require '../../../../coffee/game/lib/util'


exports.init = ( model ) ->
  #Инициализация костюмов
  #'Костюм снежного скитальца'
  itemState1 = new ItemStats 0,0,0,0,10,'Костюм снежного скитальца',1,1,1,1
  #'Утепленный костюм северного охотника'
  itemState2 = new ItemStats 0,0,0,0,10,'Утепленный костюм северного охотника',2,2,0,0
  #Порванный утепленный костюм северного охотника'
  itemState3 = new ItemStats 0,0,0,0,10,'Порванный утепленный костюм северного охотника',1,1,1,1
  #'Костюм из шкуры барса'
  itemState4 = new ItemStats 0,0,0,0,10,'Костюм из шкуры барса',5,5,0,0
  #'Сломанная броня ледянных гигантов'
  itemState5 = new ItemStats 2,0,0,0,10,'Ледяная броня',10,10,0,0
  #'Броня северного охотника'
  itemState6 = new ItemStats 1,1,0,0,10,'Броня северного охотника',6,6,0,0

  suits =[]
  suits.push itemState1
  suits.push itemState2
  suits.push itemState3
  suits.push itemState4
  suits.push itemState5
  suits.push itemState6

  itemNumber = 1 + random 5

  inv = game.inventoriesController.get( model.id )
  
  inv.addItem(new Item 1, 'john Suit', 1, suits[itemNumber])
  inv.useItemOn model, 1

  style =
    font: 'Merriweather'
    stroke: '#000000'
    fontSize: 12
  
  game.add.text 200, 0, "John suit: #{suits[itemNumber].description} armor #{suits[itemNumber].armor} movement #{suits[itemNumber].movement} maxHpBonus #{suits[itemNumber].maxHpBonus} HpBonus #{suits[itemNumber].HpBonus} attack #{suits[itemNumber].attack}", style

exports.execute = () ->
  #TODO

exports.dispose = () ->
  #TODO

