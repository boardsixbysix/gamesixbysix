_log = console.log.bind console
_log 'gulp started'

gulp = require 'gulp'
source = require 'vinyl-source-stream'
buffer = require 'vinyl-buffer'
browserify = require 'browserify'
watchify = require 'watchify'
coffeeify = require 'coffeeify'
uglify = require 'gulp-uglify'
gutil = require 'gulp-util'
minimatch = require 'minimatch'
server = require 'browser-sync'

_config =
	coffee:
		watch: [ 'src/coffee/**/**', 'src/index.html' ]
		src: 'src/coffee/start.coffee'
		dest: 'src/js/'
		uglify: no
		sourceMaps: yes
	serve:
		src: 'src/'
		port: 9000


_patternGlob = ( p )->
	if p[0] is '!'
		'!**/' + p[ 1.. ]
	else
		'**/' + p

_matchGlob = ( name, patterns )->
	if typeof patterns is 'string'
		minimatch name, _patternGlob patterns
	else
		matched = no
		for p in patterns
			isExclusion = p[0] is '!'
			if isExclusion is matched
				matched = minimatch name, _patternGlob p
		matched

_debounced = ( time, func )->
	timeout = null
	( args... )->
		later = ->
			timeout = null
			func args...
		clearTimeout timeout
		timeout = setTimeout later, time
		return

#
# bundle js
#
_bundler = browserify
	entries: _config.coffee.src
	debug: _config.coffee.sourceMaps
	transform: [ coffeeify ]
	extensions: [ '.coffee' ]
	cache: {}
	packageCache: {}

#
# watch and recompile
#
_watcher = watchify _bundler
_watcher.on 'log', gutil.log


gulp.task 'make-coffee', ->
	pipe = _watcher.bundle()
	.on 'error', ( args... )->
		gutil.log gutil.colors.red 'Browserify Error', args
	.pipe source 'start.js'
	.pipe buffer()
	pipe = pipe.pipe uglify() if _config.coffee.uglify
	pipe.pipe gulp.dest _config.coffee.dest

gulp.task 'reload', _debounced 1000, ( done )->
	server.reload()
	done()


gulp.task 'watch', ( done )->
	_watcher.on 'update', ( names )->
		gutil.log '\n\n'
		changed = []
		for n in names when _matchGlob n, _config.coffee.watch
			changed.push n
			gutil.log gutil.colors.green 'Changed file', n
		if changed.length
			do gulp.series 'make-coffee', 'reload'

	done()


gulp.task 'serve', gulp.series 'make-coffee', 'watch', ( done )->
	serve =
		port: _config.serve.port
		server: _config.serve.src
		ui:
			port: _config.serve.port + 2
		ghostMode: no
	if gutil.env.nobrowser
		serve.browser = 'none'
	server serve
	done()

gulp.task 'default', gulp.series 'serve'
